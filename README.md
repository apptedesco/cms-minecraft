# Minecraft CMS Steocorp

# Author
> ![alt text](https://crafatar.com/avatars/65b2f9ec26b04a0cbccbed03692168c1?size=64) [@steodec](https://paul-tedesco.com)

# Installation
 WIP for the moment you need to initialize you Config.php
 ````php
<?php
    class Config
    {
        const CONFIG = [
            "SITE_NAME" => "TOTO",
            "DB_HOST" => "localhost",
            "DB_NAME" => "website",
            "DB_USER" => "root",
            "DB_PASSWORD" => "",
            "DB_PORT" => "3306",
        ];
    }
````