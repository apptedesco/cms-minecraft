<section id="userList">
    <h2 class="title text-center">Liste des Slides</h2>
    <a class="btn-outline-primary btn" href="/admin/carousel/new"><i class="material-icons">add</i> Ajouter une
        slide</a>
    <table class="table table-full-width table-light">
        <tr>
            <th>id</th>
            <th>Photo</th>
            <th>Titre</th>
            <th>Sous-Titre</th>
            <th class="text-right">Actions</th>
        </tr>
        <tbody>
        <?php foreach ($this->CarousselAll as $car): ?>
            <tr>
                <td><?= $car->getId() ?></td>
                <td>
                    <div class="img-container">
                        <img style="height: 100px !important; width: auto !important;"
                             src="<?= parent::ASSETS . "img" . $car->getImg() ?>"
                             alt="">
                    </div>
                </td>
                <td><?= $car->getTitle() ?></td>
                <td><?= $car->getSubTitle() ?></td>
                <td class="text-right"><a href="/admin/carousel/edit/<?= $car->getId() ?>" rel="tooltip"
                                          class="btn btn-outline-success">
                        <i class="material-icons">edit</i></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</section>