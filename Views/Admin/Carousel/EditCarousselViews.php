<div class="container">
    <h1 class="title text-center">Modifier la slide</h1>
    <a class="btn btn-outline" href="/admin/carousel"><i class="material-icons">keyboard_arrow_left</i> Retour à la
        liste</a>

    <section id="UserForm">
        <?php if (!is_null($this->Error)): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>ERREUR:</strong> <?= $this->Error ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php if (!is_null($this->Success)): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Bravo:</strong> <?= $this->Success ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <div class="card">
            <div class="card-header">
                <h2 class="card-title text-center"><u>Slide</u></h2>
            </div>
            <div class="card-body">
                <form action="/admin/carousel/new" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Titre</label>
                        <input type="Text" id="title" name="title" class="form-control"
                               value="<?= $this->Carousel->getTitle() ?>">
                    </div>
                    <div class="form-group">
                        <label for="subtitle">Sous Titre</label>
                        <input type="text" id="subtitle" name="subtitle" class="form-control"
                               value="<?= $this->Carousel->getSubTitle() ?>">
                    </div>
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-raised">
                            <img src="<?= parent::ASSETS . "img" . $this->Carousel->getImg() ?>"
                                 rel="nofollow"
                                 alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                        <div>
                            <span class="btn btn-raised btn-round btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Changer</span>
                                <input type="file" name="image"/>
                            </span>
                            <a href="javascript:" class="btn btn-danger btn-round fileinput-exists"
                               data-dismiss="fileinput"><i class="fa fa-times"></i> Supprimer</a>
                        </div>
                    </div>
                    <input type="submit" name="submit" class="btn btn-outline-success">
                </form>
            </div>
        </div>
    </section>
</div>