<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header card-header-danger">
                <h4 class="card-title">Derniers d'utilisateurs</h4>
            </div>
            <div class="body">
                <table class="table table-full-width table-light">
                    <tr>
                        <th>Photo de Profil</th>
                        <th>Pseudo</th>
                        <th>Email</th>
                        <th class="text-right">Grade</th>
                        <th class="text-right">Actions</th>
                    </tr>
                    <tbody>
                    <?php foreach ($UserList as $user): ?>
                        <tr>
                            <td><img width="32px;"
                                     src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($user->getUsername()) ?>"
                                     alt=""></td>
                            <td>
                                <h3><?= $user->getUsername() ?></h3>
                            </td>
                            <td>
                                <?= $user->getEmail() ?>
                            </td>
                            <td class="text-right">
                                <?php switch ($user->getRole()):

                                    case "ADMIN":
                                        ?>
                                        <span class="badge badge-danger">Administrateur</span>
                                        <?php break;
                                    case"USER":
                                        ?>
                                        <span class="badge badge-info">Utilisateur</span>
                                        <?php
                                        break;

                                endswitch; ?>
                            </td>
                            <td class="td-actions text-right">
                                <a type="button" href="/admin/users/<?= $user->getId() ?>" rel="tooltip"
                                   class="btn btn-outline-success">
                                    <i class="material-icons">edit</i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Nombre d'article</h4>
            </div>
        </div>
    </div>
</div>