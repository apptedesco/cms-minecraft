</div>
</div>
</div>

<!--   Core JS Files   -->
<script src="<?= parent::ASSETS ?>js/core/jquery.min.js" type="text/javascript"></script>
<script src="<?= parent::ASSETS ?>js/core/popper.min.js" type="text/javascript"></script>
<script src="<?= parent::ASSETS ?>js/core/bootstrap-material-design.min.js" type="text/javascript"></script>

<!-- Plugin for the Perfect Scrollbar -->
<script src="<?= parent::ASSETS ?>js/plugins/perfect-scrollbar.jquery.min.js"></script>

<!-- Plugin for the momentJs  -->
<script src="<?= parent::ASSETS ?>js/plugins/moment.min.js"></script>

<!--  Plugin for Sweet Alert -->
<script src="<?= parent::ASSETS ?>js/plugins/sweetalert2.js"></script>

<!-- Forms Validations Plugin -->
<script src="<?= parent::ASSETS ?>js/plugins/jquery.validate.min.js"></script>

<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= parent::ASSETS ?>js/plugins/jquery.bootstrap-wizard.js"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?= parent::ASSETS ?>js/plugins/bootstrap-selectpicker.js"></script>

<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?= parent::ASSETS ?>js/plugins/bootstrap-datetimepicker.min.js"></script>

<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="<?= parent::ASSETS ?>js/plugins/jquery.datatables.min.js"></script>

<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="<?= parent::ASSETS ?>js/plugins/bootstrap-tagsinput.js"></script>

<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?= parent::ASSETS ?>js/plugins/jasny-bootstrap.min.js"></script>

<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="<?= parent::ASSETS ?>js/plugins/fullcalendar.min.js"></script>

<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="<?= parent::ASSETS ?>js/plugins/jquery-jvectormap.js"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?= parent::ASSETS ?>js/plugins/nouislider.min.js"></script>

<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

<!-- Library for adding dinamically elements -->
<script src="<?= parent::ASSETS ?>js/plugins/arrive.min.js"></script>

<!-- Chartist JS -->
<script src="<?= parent::ASSETS ?>js/plugins/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="<?= parent::ASSETS ?>js/plugins/bootstrap-notify.js"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= parent::ASSETS ?>js/material-dashboard.min.js?v=2.1.2" type="text/javascript"></script>

</body>
</html>