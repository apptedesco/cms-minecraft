<!doctype html>
<html lang="fr">
<head>
    <title><?= $this->getPageTitle() ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="<?= parent::ASSETS ?>css/material-dashboard.css">
    <link rel="stylesheet" href="<?= parent::ASSETS ?>css/main.css">

</head>
<body>
<div class="wrapper ">
    <div class="sidebar ps" data-color="azure" data-background-color="white">
        <div class="logo"><a href="/admin" class="simple-text logo-mini">
                <?= str_split($this->Config::CONFIG['SITE_NAME'], 1)[0] ?>
            </a>
            <a href="/admin" class="simple-text logo-normal">
                <?= $this->Config::CONFIG['SITE_NAME'] ?>
            </a></div>
        <div class="sidebar-wrapper ps">
            <div class="user">
                <div class="photo">
                    <img src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($_SESSION['user']['username']) ?>">
                </div>
                <div class="user-info">
              <span>
                <?= $_SESSION['user']['username'] ?>
              </span>
                </div>
            </div>
            <ul class="nav">

                <li class="nav-item">
                    <a class="nav-link" href="/">
                        <i class="material-icons">undo</i>
                        <p> Retour au site </p>
                    </a>
                </li>
                <li class="nav-item <?= ($this->active === "home") ? "active" : "" ?> ">
                    <a class="nav-link" href="/admin">
                        <i class="material-icons">dashboard</i>
                        <p> Accueil </p>
                    </a>
                </li>
                <li class="nav-item <?= ($this->active === "user") ? "active" : "" ?> ">
                    <a class="nav-link" href="/admin/users">
                        <i class="material-icons">person</i>
                        <p> Utilisateur </p>
                    </a>
                </li>
                <li class="nav-item <?= ($this->active === "carousel") ? "active" : "" ?> ">
                    <a class="nav-link" href="/admin/carousel">
                        <i class="material-icons">view_carousel</i>
                        <p> Carroussel </p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel ps">
        <div class="container">

