<section id="userList">
    <h2 class="title text-center">Liste des utilisateurs</h2>
    <table class="table table-full-width table-light">
        <tr>
            <th>id</th>
            <th>Photo de Profil</th>
            <th>Pseudo</th>
            <th>Email</th>
            <th class="text-right">Grade</th>
            <th class="text-right">Actions</th>
        </tr>
        <tbody>
        <?php foreach ($this->UserList as $user): ?>
            <tr>
                <td><?= $user->getId() ?></td>
                <td><img width="32px;"
                         src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($user->getUsername()) ?>"
                         alt=""></td>
                <td>
                    <h3><?= $user->getUsername() ?></h3>
                </td>
                <td>
                    <?= $user->getEmail() ?>
                </td>
                <td class="text-right">
                    <?php switch ($user->getRole()):

                        case "ADMIN":
                            ?>
                            <span class="badge badge-danger">Administrateur</span>
                            <?php break;
                        case"USER":
                            ?>
                            <span class="badge badge-info">Utilisateur</span>
                            <?php
                            break;

                    endswitch; ?>
                </td>
                <td class="td-actions text-right">
                    <a type="button" href="/admin/users/<?= $user->getId() ?>" rel="tooltip"
                       class="btn btn-outline-success">
                        <i class="material-icons">edit</i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</section>