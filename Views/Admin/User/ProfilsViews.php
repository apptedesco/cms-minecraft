<h2 class="title text-center">Edité un Profil</h2>

<a class="btn btn-outline" href="/admin/users"><i class="material-icons">keyboard_arrow_left</i> Retour à la liste</a>

<?php if (!is_null($this->Error)): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>ERREUR:</strong> <?= $this->Error ?>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>
<?php if (!is_null($this->Success)): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Bravo:</strong> <?= $this->Success ?>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>
<div class="card">
    <div class="card-header card-header-transparent">
        <i class="card-icon card-icon-transparent">
            <img width="64px;"
                 src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($this->User->getUsername()) ?>"
                 alt="">
        </i>
        <h2>
            Profile de <b><?= $this->User->getUsername() ?> </b>
        </h2>
    </div>
    <div class="card-body">
        <form action="/admin/users/update/<?= $this->User->getId() ?>" method="post">
            <div class="form-group">
                <label for="aemail">Email</label>
                <input type="email" id="aemail" name="aemail" class="form-control"
                       value="<?= $this->User->getEmail() ?>">
            </div>
            <div class="form-group">
                <label for="ausername">Pseudo Minecraft</label>
                <input type="text" id="ausername" name="ausername" class="form-control"
                       value="<?= $this->User->getUsername() ?>">
            </div>
            <div class="form-group">
                <label for="arole">Roles</label>
                <select name="arole" class="form-control selectpicker" data-style="btn btn-link" id="arole">
                    <option class="selected" selected
                            value="<?= $this->User->getRole() ?>"><?= $this->User->getRole() ?></option>
                    <option value="<?= ($this->User->getRole() == "ADMIN") ? "USER" : "ADMIN" ?>">
                        <?= ($this->User->getRole() == "ADMIN") ? "USER" : "ADMIN" ?></option>
                </select>
            </div>
            <input type="submit" name="asubmit" class="btn btn-outline-success">
            <a href="/admin/users/delete/<?= $this->User->getId() ?>"
               class="btn btn-outline-danger">Supprimer <?= $this->User->getUsername() ?></a>
        </form>

    </div>
</div>
