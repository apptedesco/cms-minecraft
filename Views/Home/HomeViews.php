<div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="https://via.placeholder.com/1920x500" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://via.placeholder.com/1920x500" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://via.placeholder.com/1920x500" alt="Third slide">
        </div>
    </div>
</div>
<div class="container">
    <section id="news">
        <div class="row" style="width: 20rem;">
            <div class="col">
                <div class="card">
                    <img class="card-img-top"
                         src="https://via.placeholder.com/260x200"
                         alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the
                            card's
                            content.</p>
                        <a href="javascript:" class="btn btn-primary">Lire la suite</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>