<!doctype html>
<html lang="fr">
<head>
    <title><?= $this->getPageTitle() ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="<?= parent::ASSETS ?>css/material-dashboard.css">
    <link rel="stylesheet" href="<?= parent::ASSETS ?>css/main.css">

</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg sticky-top bg-transparent">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo03"
                        aria-controls="navbarTogglerDemo03" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <a class="navbar-brand" href="/"><?= $this->Config::CONFIG['SITE_NAME'] ?></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/profils">Liste des membres</a>
                        </li>
                    </ul>
                </div>
                <div class="justify-content-end">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <?php if (!isset($_SESSION['user'])): ?>
                            <li class="nav-item">
                                <a href="/login" class="btn btn-outline-info">Connection</a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href=""
                                   id="navbarDropdownMenuLink"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon"><img width="32px;"
                                                         src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($_SESSION['user']['username']) ?>"
                                                         alt=""></i> <?= $_SESSION['user']['username'] ?>
                                </a>
                                <div class="dropdown-menu"
                                     aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="/profils/<?= $_SESSION['user']['id'] ?>">Mon
                                        Profils</a>
                                    <div class="dropdown-divider"></div>
                                    <?php if ($_SESSION['user']['roles'] === "ADMIN"): ?>
                                        <a class="dropdown-item" href="/admin">Administration</a>
                                    <?php endif; ?>
                                    <a class="dropdown-item btn-outline-danger" href="/logout">Déconnection</a>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>

            </div>
        </nav>

