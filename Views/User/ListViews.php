<div class="container">
    <h1 class="title text-center">Liste des membres</h1>
    <section id="userList">
        <table class="table table-full-width table-light">
            <tbody>
            <?php foreach ($this->UserList as $user): ?>
                <tr onclick="document.location.href='/profils/<?= $user->getId() ?>'" style="cursor: pointer">
                    <td><img width="32px;"
                             src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($user->getUsername()) ?>"
                             alt=""></td>
                    <td>
                        <h3><?= $user->getUsername() ?></h3>
                    </td>
                    <td>
                        <?php switch ($user->getRole()):

                            case "ADMIN":
                                ?>
                                <span class="badge badge-danger">Administrateur</span>
                                <?php break;
                            case"USER":
                                ?>
                                <span class="badge badge-info">Utilisateur</span>
                                <?php
                                break;

                        endswitch; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>