<div class="container">
    <h1 class="title text-center">Page de connection</h1>
    <section id="UserForm">
        <?php if (!is_null($this->Error)): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>ERREUR:</strong> <?= $this->Error ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <?php if (!is_null($this->Success)): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Bravo:</strong> <?= $this->Success ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-6" id="inscription">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title text-center"><u>Inscription</u></h2>
                    </div>
                    <div class="card-body">
                        <form action="/register" method="post">
                            <div class="form-group">
                                <label for="remail">Email</label>
                                <input type="email" id="remail" name="remail" class="form-control"
                                       placeholder="e.g. notch@minecraft.net">
                            </div>
                            <div class="form-group">
                                <label for="rusername">Pseudo Minecraft</label>
                                <input type="text" id="rusername" name="rusername" class="form-control"
                                       placeholder="e.g. Notch">
                            </div>
                            <div class="form-group">
                                <label for="rpassword">Mot de passe</label>
                                <input type="password" id="rpassword" name="rpassword" class="form-control"
                                       placeholder="e.g. P@ssw0rd">
                            </div>
                            <div class="form-group">
                                <label for="rconfirm">Confirmation du mot de passe</label>
                                <input type="password" id="rconfirm" name="rconfirm" class="form-control"
                                       placeholder="e.g. P@ssw0rd">
                            </div>
                            <input type="submit" name="rsubmit" class="btn btn-outline-success">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-6" id="connexion">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title text-center"><u>Connection</u></h2>
                    </div>
                    <div class="card-body">
                        <form action="/connection" method="post">
                            <div class="form-group">
                                <label for="clogin">Email ou Pseudo</label>
                                <input type="text" id="clogin" name="clogin" class="form-control"
                                       placeholder="e.g. notch@minecraft.net">
                            </div>
                            <div class="form-group">
                                <label for="cpassword">Mot de passe</label>
                                <input type="password" id="cpassword" name="cpassword" class="form-control"
                                       placeholder="e.g. P@ssw0rd">
                            </div>
                            <input type="submit" name="csubmit" class="btn btn-outline-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>