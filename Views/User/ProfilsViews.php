<div class="container">

    <?php if (!is_null($this->Error)): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>ERREUR:</strong> <?= $this->Error ?>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (!is_null($this->Success)): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Bravo:</strong> <?= $this->Success ?>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <div class="card">
        <div class="card-header card-header-transparent">
            <i class="card-icon card-icon-transparent">
                <img width="64px;"
                     src="https://crafatar.com/avatars/<?= $this->Minecraft->username_to_uuid($this->User->getUsername()) ?>"
                     alt="">
            </i>
            <h2>
                Profile de <b><?= $this->User->getUsername() ?> </b>
            </h2>
        </div>
        <div class="card-body">
            <?php if (isset($_SESSION['user']) &&
            $_SESSION['user']['id'] == $this->User->getID()): ?>
            <form action="/update/<?= $this->User->getId() ?>" method="post">
                <?php endif; ?>
                <div class="form-group">
                    <label for="pemail">Email</label>
                    <input type="email" id="pemail" name="pemail" class="form-control"
                           value="<?= $this->User->getEmail() ?>" <?= (!isset($_SESSION['user']) ||
                        $_SESSION['user']['id'] != $this->User->getID()) ? "readonly" : "" ?>>
                </div>
                <div class="form-group">
                    <label for="pusername">Pseudo Minecraft</label>
                    <input type="text" id="pusername" name="pusername" class="form-control"
                           value="<?= $this->User->getUsername() ?>" <?= (!isset($_SESSION['user']) ||
                        $_SESSION['user']['id'] != $this->User->getID()) ? "readonly" : "" ?>>
                </div>
                <div class="form-group">
                    <label for="prole">Roles</label>
                    <input type="text" id="prole" name="prole" class="form-control"
                           value="<?= $this->User->getRole() ?>" readonly>
                </div>
                <?php if (isset($_SESSION['user']) && $_SESSION['user']['id'] == $this->User->getID()): ?>
                    <div class="form-group">
                        <label for="ppassword">Ancien mot de passe</label>
                        <input type="password" id="ppassword" name="ppassword" class="form-control"
                               placeholder="e.g. P@ssw0rd">
                    </div>
                    <div class="form-group">
                        <label for="pnpassword">Nouveaux mot de passe</label>
                        <input type="password" id="pnpassword" name="pnpassword" class="form-control"
                               placeholder="e.g. P@ssw0rd">
                    </div>
                    <div class="form-group">
                        <label for="pconfirm">Confirmation</label>
                        <input type="password" id="pconfirm" name="pconfirm" class="form-control"
                               placeholder="e.g. P@ssw0rd">
                    </div>
                    <input type="submit" name="psubmit" class="btn btn-outline-success">
                <?php endif; ?>
                <?php if (isset($_SESSION['user']) &&
                $_SESSION['user']['id'] == $this->User->getID()): ?>
            </form>
        <?php endif; ?>

        </div>
    </div>
</div>