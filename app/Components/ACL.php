<?php

    namespace App\Components;

    class ACL
    {
        public function isAdmin ()
        {
            if (isset($_SESSION['user'])):
                switch ($_SESSION['user']['roles']):
                    case "ADMIN":
                        return TRUE;
                    case "USER":
                        return FALSE;
                endswitch;
            endif;
            return FALSE;
        }
    }
