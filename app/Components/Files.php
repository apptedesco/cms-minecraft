<?php

    namespace App\Components;

    use ErrorException;
    use RuntimeException;

    class Files
    {
        public function UploadImage ($file)
        {

            $target_dir = "../Assets/img/uploads/";
            $target_file = $target_dir . basename($file["name"]);

            try {

                // Undefined | Multiple Files | $_FILES Corruption Attack
                // If this request falls under any of them, treat it invalid.
                if (
                    !isset($file['error']) ||
                    is_array($file['error'])
                ) {
                    return new ErrorException('Invalid parameters.');
                }

                // Check $file['error'] value.
                switch ($file['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        return new ErrorException('No file sent.');
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        return new ErrorException('Exceeded filesize limit.');
                    default:
                        return new ErrorException('Unknown errors.');
                }

                // You should also check filesize here.
                if ($file['size'] > 1000000) {
                    return new ErrorException('Exceeded filesize limit.');
                }

                // DO NOT TRUST $file['mime'] VALUE !!
                // Check MIME Type by yourself.
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (FALSE === $ext = array_search(
                        $finfo->file($file['tmp_name']),
                        array(
                            'jpg' => 'image/jpeg',
                            'png' => 'image/png',
                            'gif' => 'image/gif',
                        ),
                        TRUE
                    )) {
                    return new ErrorException('Invalid file format.');
                }

                // You should name it uniquely.
                // DO NOT USE $file['name'] WITHOUT ANY VALIDATION !!
                // On this example, obtain safe unique name from its binary data.
                if (!move_uploaded_file(
                    $file['tmp_name'],
                    sprintf($_SERVER['DOCUMENT_ROOT'] . '/Assets/img/upload/%s',
                        $file['name']
                    )

                )) {
                    return new ErrorException('Failed to move uploaded file.');
                }

                return NULL;

            } catch (RuntimeException $e) {

                echo $e->getMessage();

            }
        }
    }