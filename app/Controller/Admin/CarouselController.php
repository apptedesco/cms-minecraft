<?php
    namespace App\Controller\Admin;

    use App\Components\Files;
    use App\Config;
    use App\Controller\Common;
    use App\Models\CarouselModels;

    /**
     * Class CarouselController
     */
    class CarouselController extends Common
    {
        /**
         * @var Config
         */
        protected $Config;
        protected $Error;
        protected $Success;
        protected $CarousselAll;
        protected $Carousel;

        public function __construct ()
        {
            $this->Config = new Config();
            parent::__construct('CarouselController Accueil | ' . $this->Config::CONFIG['SITE_NAME'], '');
        }

        public function ReturnView ()
        {
            $Caroussel = new CarouselModels();
            $this->CarousselAll = $Caroussel->getAll();
            $this->active = "carousel";
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/Carousel/CarouselViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
        }

        public function ReturnAddView ()
        {
            $this->active = "carousel";
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/Carousel/NewCarousselViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
        }

        public function NewCarousel ()
        {
            $uploadImage = new Files();
            if (is_null($_POST['submit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                return self::ReturnAddView();
            endif;
            if (is_null($_FILES['image']) || $_FILES['image']['name'] === ""):
                $this->Error = "L'image ne peut être vide";
                return self::ReturnAddView();
            endif;
            $err = $uploadImage->UploadImage($_FILES['image']);
            if (!is_null($err)):
                $this->Error = $err->getMessage();
                return self::ReturnAddView();
            endif;
            $carousel = new CarouselModels();
            $location = "/upload/" . $_FILES['image']['name'];
            $carousel->setImg($location);
            if ($_POST['title'] != "") $carousel->setTitle($_POST['title']);
            if ($_POST['subtitle'] != "") $carousel->setSubTitle($_POST['subtitle']);
            $carousel->createCarousel();
            $this->Success = "Nouveaux Slide enregistrer";
            return self::ReturnAddView();
        }

        public function EditCaroussel ($id)
        {
            $this->active = "carousel";
            $this->Carousel = new CarouselModels();
            $this->Carousel->setId($id);
            $this->Carousel->getOneByID();

            if (is_null($_POST['submit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                return self::ReturnAddView();
            endif;

            if (is_null($_FILES['image']) || $_FILES['image']['name'] === ""):
                $this->Error = "L'image ne peut être vide";
                return self::ReturnAddView();
            endif;
        }

        public function ReturnEditView ($id)
        {
            $this->active = "carousel";
            $this->Carousel = new CarouselModels();
            $this->Carousel->setId($id);
            $this->Carousel->getOneByID();
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/Carousel/EditCarousselViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
        }
    }