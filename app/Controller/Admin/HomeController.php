<?php
    /**
     * Created by PhpStorm.
     * User: pault
     * Date: 04/03/2019
     * Time: 12:25
     */
    namespace App\Controller\Admin;

    use App\Config;
    use App\Controller\Common;
    use App\Models\UsersModels;

    class HomeController extends Common
    {
        protected Config $Config;

        public function __construct ()
        {
            $this->Config = new Config();
            parent::__construct('Admin Accueil | ' . $this->Config::CONFIG['SITE_NAME'], '');
        }

        public function ReturnView ()
        {
            $this->active = "home";
            $user = new UsersModels();
            $UserList = array_slice($user->getAllUser(), 0, ($user->getAllUser() > 5) ? 5 : count($user->getAllUser()));
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/Home/HomeViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
        }
    }