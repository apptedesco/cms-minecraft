<?php
    namespace App\Controller\Admin;

    use App\Config;
    use App\Controller\Common;
    use App\Models\UsersModels;

    /**
     * Class UserController
     */
    class UserController extends Common
    {
        /**
         * @var Config
         */
        protected $Config;
        /**
         * @var string
         */
        protected $Error;
        /**
         * @var string
         */
        protected $Success;
        /**
         * @var UsersModels
         */
        protected $User;
        /**
         * @var UsersModels[]
         */
        protected $UserList;

        /**
         * UserController constructor.
         */
        public function __construct ()
        {
            $this->active = "user";
            $this->Config = new Config();
            parent::__construct("Utilisateur", '');
        }

        /**
         * @author pault
         */
        public function ReturnView (): void
        {
            $user = new UsersModels();
            $this->UserList = $user->getAllUser();
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/User/ListViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
            return;
        }

        /**
         * @param $id
         * @author pault
         */
        public function ReturnProfilsViews ($id)
        {
            $p = new UsersModels();
            $p->setId($id);
            $exist = $p->getUserByID();
            if ($exist):
                $this->User = $p;
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD_ADMIN;
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Admin/User/ProfilsViews.php';
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER_ADMIN;
            else:
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Errors/404.html';
            endif;
        }

        public function UpdateUser ($id)
        {
            if (is_null($_POST['asubmit']) || !isset($_POST['asubmit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                self::ReturnProfilsViews($id);
                return;
            endif;
            if (is_null($_POST['ausername']) || $_POST['ausername'] == ""):
                $this->Error = "Le pseudo ne peut pas être vide";
                self::ReturnProfilsViews($id);
                return;
            endif;
            if (is_null($_POST['aemail']) || $_POST['aemail'] == ""):
                $this->Error = "L'email ne peut pas être vide";
                self::ReturnProfilsViews($id);
                return;
            endif;
            if (is_null($_POST['arole']) || $_POST['arole'] == ""):
                $this->Error = "Le role ne peut pas être vide";
                self::ReturnProfilsViews($id);
                return;
            endif;
            $u = new UsersModels();
            $u->setUsername($_POST['ausername']);
            $u->setEmail($_POST['aemail']);
            $u->setRole($_POST['arole']);
            $u->setId($id);
            $query = $u->updateUser();
            if (is_null($query)):
                $this->Success = "L'utilisateur a bien été modifié";
                self::ReturnProfilsViews($id);
                return;
            else:
                $this->Error = $query->getMessage();
                self::ReturnProfilsViews($id);
                return;
            endif;
        }


        public function DeleteUser ($id)
        {
            $u = new UsersModels();
            $u->setId($id);
            $err = $u->deleteUser();
            if (is_null($err)):
                echo "<script>document.location.href = '/admin/users'</script>";
                return;
            else:
                $this->Error = $err->getMessage();
                return;
            endif;
        }
    }