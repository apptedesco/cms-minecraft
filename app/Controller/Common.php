<?php
    namespace App\Controller;

    use App\Components\APIMinecraft;

    class Common
    {

#----------------------------------------
#-------- Definition des PATH -----------
#-------- Head Header Footer  -----------
#----------------------------------------
        const HEAD = "/Views/Required/HeadViews.php";
        const HEAD_ADMIN = "/Views/Admin/Required/HeadViews.php";
        const FOOTER = "/Views/Required/FooterViews.php";
        const FOOTER_ADMIN = "/Views/Admin/Required/FooterViews.php";
        const VIEWS = "/Views/";
        const ASSETS = "/Assets/";

        private $pageTitle;
        private $pageDesc;
        public $Minecraft;

        public $active;

#----------------------------------------
#---- Définition du Titre de la page ----
#----------------------------------------

        public function __construct ($pageTitle, $pageDesc)
        {
            $this->Minecraft = new APIMinecraft();
            $this->pageTitle = $pageTitle;
            $this->pageDesc = $pageDesc;
        }

        public function getPageTitle ()
        {
            return $this->pageTitle;
        }

        public function getPageDesc ()
        {
            return $this->pageDesc;
        }
    }
