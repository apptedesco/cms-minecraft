<?php
    /**
     * Created by PhpStorm.
     * User: pault
     * Date: 04/03/2019
     * Time: 12:25
     */
    namespace App\Controller;
    use App\Config;

    class HomeController extends Common
    {
        protected $Config;

        public function __construct ()
        {
            $this->Config = new Config();
            parent::__construct('Accueil | ' . $this->Config::CONFIG['SITE_NAME'], '');
        }

        public function ReturnView ()
        {
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Home/HomeViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER;
        }
    }