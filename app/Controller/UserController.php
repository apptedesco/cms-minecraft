<?php
    namespace App\Controller;

    use App\Config;
    use App\Models\UsersModels;

    /**
     * Class UserController
     */
    class UserController extends Common
    {
        /**
         * @var Config
         */
        protected $Config;
        /**
         * @var string
         */
        protected $Error;
        /**
         * @var string
         */
        protected $Success;
        /**
         * @var UsersModels
         */
        protected $User;
        /**
         * @var UsersModels[]
         */
        protected $UserList;

        /**
         * UserController constructor.
         */
        public function __construct ()
        {
            $this->Config = new Config();
            parent::__construct("Utilisateur", '');
        }

        /**
         * @author pault
         */
        public function ReturnView (): void
        {
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'User/LoginViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER;
            return;
        }

        /**
         * @param $id
         * @author pault
         */
        public function ReturnProfilsViews ($id)
        {
            $p = new UsersModels();
            $p->setId($id);
            $exist = $p->getUserByID();
            if ($exist):
                $this->User = $p;
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD;
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'User/ProfilsViews.php';
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER;
            else:
                require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'Errors/404.html';
            endif;
        }

        /**
         * @author pault
         */
        public function ReturnProfilsListViews ()
        {
            $p = new UsersModels();
            $this->UserList = $p->getAllUser();
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::HEAD;
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::VIEWS . 'User/ListViews.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . parent::FOOTER;
        }

        /**
         * @author pault
         */
        public function Connect (): void
        {
            if (is_null($_POST['csubmit']) || !isset($_POST['csubmit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['clogin']) || $_POST['clogin'] == ""):
                $this->Error = "L'email ou le pseudo ne peut pas être vide";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['cpassword']) || $_POST['cpassword'] == ""):
                $this->Error = "Le mot de passe ne peut pas être vide";
                self::ReturnView();
                return;
            endif;

            $l = new UsersModels();

            $l->setUsername($_POST['clogin']);
            $l->setPassword($_POST['cpassword']);

            $con = $l->loginUser();
            if ($con):
                $this->Success = "Félécitation vous êtes connecté";
                self::ReturnView();
                return;
            else:
                $this->Error = "Mot de passe ou email ou pseudo inconnue";
                self::ReturnView();
                return;
            endif;
        }

        /**
         * @author pault
         */
        public function Register (): void
        {
            if (is_null($_POST['rsubmit']) || !isset($_POST['rsubmit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['remail']) || $_POST['remail'] == ""):
                $this->Error = "L'email ne peut pas être vide";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['rusername']) || $_POST['rusername'] == ""):
                $this->Error = "Le nom d'utilisateur ne peut pas être vide";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['rpassword']) || $_POST['rpassword'] == ""):
                $this->Error = "Le mot de passe ne peut pas être vide";
                self::ReturnView();
                return;
            endif;
            if (is_null($_POST['rconfirm']) || $_POST['rpassword'] != $_POST['rconfirm']):
                $this->Error = "Le mot de passe et la confirmation ne sont pas identique";
                self::ReturnView();
                return;
            endif;

            $u = new UsersModels();

            $u->setEmail($_POST['remail']);
            $u->setUsername($_POST['rusername']);
            $u->setPassword($_POST['rpassword']);

            $err = $u->createUser();
            if (is_null($err) || $err->getMessage() == ""):
                $this->Success = "Félécitation vous avez créer votre compte";
                self::ReturnView();
                return;
            else:
                $this->Error = $err->getMessage();
                self::ReturnView();
                return;
            endif;
        }

        /**
         * @param $id
         * @author pault
         */
        public function UpdateUser ($id)
        {
            if (is_null($_POST['psubmit']) || !isset($_POST['psubmit'])):
                $this->Error = "Une erreur est survenue lors de l'envoie du formulaire";
                self::ReturnProfilsViews($id);
                return;
            endif;
            if (is_null($_POST['pusername']) || $_POST['pusername'] == ""):
                $this->Error = "L'email ou le pseudo ne peut pas être vide";
                self::ReturnProfilsViews($id);
                return;
            endif;
            if (is_null($_POST['pemail']) || $_POST['pemail'] == ""):
                $this->Error = "L'email ou le pseudo ne peut pas être vide";
                self::ReturnProfilsViews($id);
                return;
            endif;
            $u = new UsersModels();
            $u->setUsername($_POST['pusername']);
            $u->setEmail($_POST['pemail']);
            $u->setId($id);
            if (!is_null($_POST['pnpassword']) && $_POST['pnpassword'] != ""):
                if ($_POST['pnpassword'] != $_POST['pconfirm']) {
                    $this->Error = "Le nouveaux mot de passe est différent de la confirmation";
                    self::ReturnProfilsViews($id);
                    return;
                }
                $u->setPassword($_POST['ppassword']);
                $u->setNPassword($_POST['pnpassword']);
            endif;
            $query = $u->updateUser();
            if ($query->getMessage() == ""):
                $this->Success = "L'utilisateur a bien été modifié";
                self::ReturnProfilsViews($id);
                return;
            else:
                $this->Error = $query->getMessage();
                self::ReturnProfilsViews($id);
                return;
            endif;
        }
    }