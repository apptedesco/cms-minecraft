<?php
    namespace App\Models;
    require_once 'DatabaseConnexion.php';

    class CarouselModels extends DatabaseConnexion
    {

        private $id;
        private $title;
        private $subTitle;
        private $img;

        public function __construct ()
        {
            parent::__construct();
        }

        /**
         * @return mixed
         */
        public function getId ()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId ($id): void
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getTitle ()
        {
            return $this->title;
        }

        /**
         * @param mixed $title
         */
        public function setTitle ($title): void
        {
            $this->title = $title;
        }

        /**
         * @return mixed
         */
        public function getSubTitle ()
        {
            return $this->subTitle;
        }

        /**
         * @param mixed $subTitle
         */
        public function setSubTitle ($subTitle): void
        {
            $this->subTitle = $subTitle;
        }

        /**
         * @return mixed
         */
        public function getImg ()
        {
            return $this->img;
        }

        /**
         * @param mixed $img
         */
        public function setImg ($img): void
        {
            $this->img = $img;
        }

        public function createCarousel ()
        {

            $query = "INSERT INTO carousel (`id`, `title`, `subtitle`, `image`)" .
                " VALUES (UUID(), :title, :subtitle, :image)";
            $createC = $this->db->prepare($query);
            $createC->bindParam(':title', $this->title, PDO::PARAM_STR);
            $createC->bindParam(':subtitle', $this->subTitle, PDO::PARAM_STR);
            $createC->bindParam(':image', $this->img, PDO::PARAM_STR);

            if (!$createC->execute()):
                return new ErrorException("Une erreur est survenue lors de l'enregistrement");
            endif;

            return NULL;
        }

        public function getAll ()
        {
            $query = "SELECT * FROM carousel";
            $getAllC = $this->db->prepare($query);
            if ($getAllC->execute()):
                $CarouselList = [];
                foreach ($getAllC->fetchAll() as $carousel):
                    $CarouselList[$carousel['id']] = new self();
                    $CarouselList[$carousel['id']]->setId($carousel['id']);
                    $CarouselList[$carousel['id']]->setImg($carousel['image']);
                    $CarouselList[$carousel['id']]->setSubTitle($carousel['title']);
                    $CarouselList[$carousel['id']]->setTitle($carousel['subtitle']);
                endforeach;
                return $CarouselList;
            endif;
            return NULL;
        }

        public function getOneByID ()
        {
            $query = "SELECT * FROM carousel WHERE id = :id";
            $getAllC = $this->db->prepare($query);
            $getAllC->bindParam(":id", $this->id, PDO::PARAM_STR);
            if ($getAllC->execute()):
                $CarouselList = $getAllC->fetchAll();
                $carousel = $CarouselList[0];
                $this->setId($carousel['id']);
                $this->setImg($carousel['image']);
                $this->setSubTitle($carousel['title']);
                $this->setTitle($carousel['subtitle']);
                return $this;
            endif;
            return NULL;
        }

    }