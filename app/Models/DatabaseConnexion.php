<?php

    namespace App\Models;

    use App\Config;
    use PDO;
    use PDOException;

    /**
     * Class DatabaseConnexion
     */
    abstract class DatabaseConnexion
    {
        /**
         * @var PDO
         */
        protected $db;
        private $Config;

        /**
         * DatabaseConnexion constructor.
         */
        protected function __construct ()
        {
            $this->Config = new Config();
            try {
                $this->db = new PDO("mysql:dbname=" . $this->Config::CONFIG['DB_NAME'] .
                    ";host=" . $this->Config::CONFIG['DB_HOST'] .
                    ";port=" . $this->Config::CONFIG['DB_PORT'],
                    $this->Config::CONFIG['DB_USER'],
                    $this->Config::CONFIG['DB_PASSWORD']);
            } catch (PDOException $e) {
                return 'Connexion échouée : ' . $e->getMessage();
            }
        }
    }
