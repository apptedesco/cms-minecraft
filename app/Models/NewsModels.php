<?php
    namespace App\Models;

    require_once 'DatabaseConnexion.php';

    /**
     * Class NewsModels
     */
    class NewsModels extends DatabaseConnexion
    {
        /**
         * @var
         */
        private $id;
        /**
         * @var
         */
        private $title;
        /**
         * @var
         */
        private $content;
        /**
         * @var
         */
        private $image;

        /**
         * NewsModels constructor.
         */
        public function __construct ()
        {
            parent::__construct();
        }

        /**
         * @return mixed
         */
        public function getId ()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId ($id): void
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getTitle ()
        {
            return $this->title;
        }

        /**
         * @param mixed $title
         */
        public function setTitle ($title): void
        {
            $this->title = $title;
        }

        /**
         * @return mixed
         */
        public function getContent ()
        {
            return $this->content;
        }

        /**
         * @param mixed $content
         */
        public function setContent ($content): void
        {
            $this->content = $content;
        }

        /**
         * @return mixed
         */
        public function getImage ()
        {
            return $this->image;
        }

        /**
         * @param mixed $image
         */
        public function setImage ($image): void
        {
            $this->image = $image;
        }

    }