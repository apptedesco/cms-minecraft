<?php

    namespace App\Models;

    use ErrorException;
    use PDO;

    /**
     * Class UsersModels
     */
    class UsersModels extends DatabaseConnexion
    {
        /**
         * @var string
         */
        private $id;
        /**
         * @var string
         */
        private $username;

        /**
         * @var string
         */
        private $password;
        /**
         * @var string
         */
        private $email;

        /**
         * @var string
         */
        private $nPassword;

        /**
         * @param mixed $nPassword
         */
        public function setNPassword ($nPassword): void
        {
            $this->nPassword = $nPassword;
        }

        public function getNPassword (): void
        {
            $this->nPassword;
        }

        /**
         * @var string
         */
        private $role = "USER";

        public function __construct ()
        {
            parent::__construct();
        }

        /**
         * @return string
         */
        public function getRole (): string
        {
            return $this->role;
        }

        /**
         * @param string $role
         */
        public function setRole (string $role): void
        {
            $this->role = $role;
        }

        /**
         * @return string
         */
        public function getUsername (): string
        {
            return $this->username;
        }

        /**
         * @param string $id
         */
        public function setId (string $id): void
        {
            $this->id = $id;
        }

        /**
         * @param string $username
         */
        public function setUsername (string $username): void
        {
            $this->username = $username;
        }

        /**
         * @param string $password
         */
        public function setPassword (string $password): void
        {
            $this->password = $password;
        }

        /**
         * @param string $email
         */
        public function setEmail (string $email): void
        {
            $this->email = $email;
        }

        /**
         * @return string
         */
        public function getPassword (): string
        {
            return $this->password;
        }

        /**
         * @return string
         */
        public function getEmail (): string
        {
            return $this->email;
        }

        /**
         * @return string
         */
        public function getId (): string
        {
            return $this->id;
        }

        /**
         * @return ErrorException|null
         * @author pault
         */
        public function createUser (): ?ErrorException
        {
            if (is_null($this->username) || $this->username === "") return new ErrorException("Le nom d'utilisateur est vide");
            if (is_null($this->password) || $this->password === "") return new ErrorException("Le mot de passe est vide");
            if (is_null($this->email) || $this->email === "") return new ErrorException("L'email est vide");

            if (is_null($this->role) || $this->role === "") $this->role = "USER";

            $this->setPassword(password_hash($this->password, PASSWORD_DEFAULT));

            $query = "INSERT INTO users (`id`, `username`, `email`, `password`, `roles`)" .
                " VALUES (UUID(), :username, :email, :password, :roles)";
            $createU = $this->db->prepare($query);
            $createU->bindParam(':username', $this->username, PDO::PARAM_STR);
            $createU->bindParam(':email', $this->email, PDO::PARAM_STR);
            $createU->bindParam(':password', $this->password, PDO::PARAM_STR);
            $createU->bindParam(':roles', $this->role, PDO::PARAM_STR);

            if (!$createU->execute()):
                return new ErrorException("Une erreur est survenue lors de l'enregistrement");
            endif;

            return NULL;
        }

        /**
         * @return ErrorException|null
         * @author pault
         */
        public function deleteUser (): ?ErrorException
        {
            $query = "DELETE FROM users WHERE id = :id";
            $deleteU = $this->db->prepare($query);
            $deleteU->bindParam(':id', $this->id, PDO::PARAM_STR);
            if ($deleteU->execute()):
                return NULL;
            else:
                return new ErrorException("Une erreur inatendue est arrivé lors de la suppréssion");
            endif;
        }

        /**
         * @return ErrorException
         * @author pault
         */
        public function updateUser (): ?ErrorException
        {
            $isUsername = FALSE;
            $isEmail = FALSE;
            $isPassword = FALSE;
            $isRole = FALSE;
            $oldUser = new self();
            $oldUser->setId($this->id);
            $oldUser->getUserByID();
            $query = "UPDATE users SET ";
            if (is_null($this->username) || $this->username != ""):
                $query = $query . "`username` = :username, ";
                $isUsername = TRUE;
            endif;
            if (is_null($this->email) || $this->email != ""):
                $query = $query . "`email` = :email ";
                $isRole = TRUE;
            endif;
            if (is_null($this->role) || $this->role != ""):
                $query = $query . ",`roles` = :roles ";
                $isEmail = TRUE;
            endif;
            if (!is_null($this->password) && $this->password != ""):
                if (!password_verify($this->password, $oldUser->password)) return new ErrorException("Le mot de passe est incorrect");
                if ($this->nPassword == $oldUser->password) return new ErrorException("Le nouveaux mot de passe ne peux être équal à l'ancien");
                $query = $query . ",`password` = :password ";
                $isPassword = TRUE;
            endif;
            $query = $query . "WHERE id = :id";
            $UpdateU = $this->db->prepare($query);
            if ($isUsername) $UpdateU->bindParam(":username", $this->username, PDO::PARAM_STR);
            if ($isEmail) $UpdateU->bindParam(":email", $this->email, PDO::PARAM_STR);
            if ($isRole) $UpdateU->bindParam(":roles", $this->role, PDO::PARAM_STR);
            if ($isPassword) {
                $password_hash = password_hash($this->nPassword, PASSWORD_DEFAULT);
                $UpdateU->bindParam(":password", $password_hash, PDO::PARAM_STR);
            }
            $UpdateU->bindParam(":id", $this->id, PDO::PARAM_STR);
            if ($UpdateU->execute()):
                self::getUserByID();
                if ($_SESSION['user']['id'] == $this->id):
                    $_SESSION['user']['id'] = self::getId();
                    $_SESSION['user']['username'] = self::getUsername();
                    $_SESSION['user']['email'] = self::getEmail();
                    $_SESSION['user']['password'] = self::getPassword();
                    $_SESSION['user']['roles'] = self::getRole();
                endif;
                return NULL;
            else:
                return new ErrorException("Une Erreur est survenue lors de la mise à jours");
            endif;
        }

        /**
         * @return UsersModels[]
         * @author pault
         */
        public function getAllUser (): array
        {
            $query = "SELECT id, username, email, roles FROM users ORDER BY created_at DESC";
            $listU = $this->db->prepare($query);

            $listU->execute();
            $UserList = [];
            foreach ($listU->fetchAll() as $user):
                $UserList[$user['id']] = new UsersModels();
                $UserList[$user['id']]->setId($user['id']);
                $UserList[$user['id']]->setRole($user['roles']);
                $UserList[$user['id']]->setEmail($user['email']);
                $UserList[$user['id']]->setUsername($user['username']);
            endforeach;
            return $UserList;
        }

        public function loginUser ()
        {
            $query = "SELECT * FROM users WHERE `email` = :login OR `username` = :login LIMIT 1";
            $loginU = $this->db->prepare($query);
            $loginU->bindParam(':login', $this->username, PDO::PARAM_STR);
            if ($loginU->execute()):
                $allU = $loginU->fetchAll();
                $u = $allU[0];
                if (!isset($u)) return FALSE;
                if (!password_verify($this->password, $u['password'])) return FALSE;
                $_SESSION['user'] = $u;
                return TRUE;
            else:
                return FALSE;
            endif;
        }

        public function getUserByID ()
        {
            if (is_null($this->id) || $this->id == "") return FALSE;
            $query = "SELECT * FROM users WHERE id = :id LIMIT 1";
            $getProfil = $this->db->prepare($query);
            $getProfil->bindParam(":id", $this->id, PDO::PARAM_STR);
            if ($getProfil->execute()):
                $allU = $getProfil->fetchAll();
                if (isset($allU[0])):
                    $u = $allU[0];
                    if (!isset($u)) return FALSE;
                    $this->setEmail($u['email']);
                    $this->setUsername($u['username']);
                    $this->setPassword($u['password']);
                    $this->setRole($u['roles']);
                    return TRUE;
                else:
                    return FALSE;
                endif;
            else:
                return FALSE;
            endif;

        }

    }