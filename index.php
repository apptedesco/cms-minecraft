<?php
    session_start();

    require_once './vendor/autoload.php';

    /**
     * Created by PhpStorm.
     * User: pault
     * Date: 09/03/2017
     * Time: 11:10
     */

    use App\Controller\HomeController;
    use App\Controller\UserController;

    use App\Controller\Admin\HomeController as AdminHome;
    use App\Controller\Admin\CarouselController as AdminCarousel;
    use App\Controller\Admin\UserController as AdminUser;

    use App\Components\ACL;

    use App\Router\Router;

    $acl = new ACL();

    $router = new Router($_GET['url']);

    $router->get('/', function () {
        header('location: accueil');
        exit();
    });
    $router->get('/home', function () {
        header('location: accueil');
        exit();
    });
    /*-----------------------------------------------------------------*/
    /*------------------------ public zone ----------------------------*/
    /*-----------------------------------------------------------------*/
    $router->get('/accueil', function () {
        $home = new HomeController();
        $home->ReturnView();
    });

    /*-----------------------------------------------------------------*/
    /*------------------------- users zone ----------------------------*/
    /*-----------------------------------------------------------------*/

    $router->get('/login', function () {
        $user = new UserController();
        $user->ReturnView();
    });
    $router->post('/connection', function () {
        $user = new UserController();
        $user->Connect();
    });
    $router->post('/register', function () {
        $user = new UserController();
        $user->Register();
    });
    $router->get('/logout', function () {
        session_destroy();
        echo "<script>document.location.href = '/login'</script>";
    });
    $router->get('/profils/:id', function ($id) {
        $user = new UserController();
        $user->ReturnProfilsViews($id);
    });
    $router->get('/profils', function () {
        $user = new UserController();
        $user->ReturnProfilsListViews();
    });
    $router->post('/update/:id', function ($id) {
        $user = new UserController();
        $user->UpdateUser($id);
    });

    /*-----------------------------------------------------------------*/
    /*------------------------ Admin zone -----------------------------*/
    /*-----------------------------------------------------------------*/

    $router->get('/admin', function () use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $admin = new AdminHome();
            $admin->ReturnView();
        endif;
    });
    $router->get('/admin/users', function () use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $user = new AdminUser();
            $user->ReturnView();
        endif;
    });
    $router->get('/admin/users/:id', function ($id) use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $user = new AdminUser();
            $user->ReturnProfilsViews($id);
        endif;
    });
    $router->post('/admin/users/update/:id', function ($id) use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $user = new AdminUser();
            $user->UpdateUser($id);
        endif;
    });
    $router->get('/admin/users/delete/:id', function ($id) use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $user = new AdminUser();
            $user->DeleteUser($id);
        endif;
    });
    $router->get('/admin/carousel', function () use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $carousel = new AdminCarousel();
            $carousel->ReturnView();
        endif;
    });
    $router->get('/admin/carousel/new', function () use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $carousel = new AdminCarousel();
            $carousel->ReturnAddView();
        endif;
    });
    $router->get('/admin/carousel/edit/:id', function ($id) use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $carousel = new AdminCarousel();
            $carousel->ReturnEditView($id);
        endif;
    });
    $router->post('/admin/carousel/new', function () use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $carousel = new AdminCarousel();
            $carousel->NewCarousel();
        endif;
    });
    $router->post('/admin/carousel/edit/:id', function ($id) use ($acl) {
        if (!$acl->isAdmin()) :
            require_once 'Views/Errors/404.html';
        else:
            $carousel = new AdminCarousel();
            $carousel->EditCaroussel($id);
        endif;
    });

    /* ----------------------------------------------------------------------------------- */


    $router->get('/:unknow', function () {
        require_once 'Views/Errors/404.html';
    });

    $router->run();
